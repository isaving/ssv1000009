package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUIS0I struct {
	//输入是个map
}

type DAACUIS0O struct {

}

type DAACUIS0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUIS0I
}

type DAACUIS0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUIS0O
}

type DAACUIS0RequestForm struct {
	Form []DAACUIS0IDataForm
}

type DAACUIS0ResponseForm struct {
	Form []DAACUIS0ODataForm
}

// @Desc Build request message
func (o *DAACUIS0RequestForm) PackRequest(DAACUIS0I DAACUIS0I) (responseBody []byte, err error) {

	requestForm := DAACUIS0RequestForm{
		Form: []DAACUIS0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUIS0I",
				},
				FormData: DAACUIS0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUIS0RequestForm) UnPackRequest(request []byte) (DAACUIS0I, error) {
	DAACUIS0I := DAACUIS0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUIS0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUIS0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUIS0ResponseForm) PackResponse(DAACUIS0O DAACUIS0O) (responseBody []byte, err error) {
	responseForm := DAACUIS0ResponseForm{
		Form: []DAACUIS0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUIS0O",
				},
				FormData: DAACUIS0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUIS0ResponseForm) UnPackResponse(request []byte) (DAACUIS0O, error) {

	DAACUIS0O := DAACUIS0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUIS0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUIS0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUIS0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
