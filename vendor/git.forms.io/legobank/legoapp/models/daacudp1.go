package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUDP1I struct {
	//输入是个map
}

type DAACUDP1O struct {

}

type DAACUDP1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUDP1I
}

type DAACUDP1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUDP1O
}

type DAACUDP1RequestForm struct {
	Form []DAACUDP1IDataForm
}

type DAACUDP1ResponseForm struct {
	Form []DAACUDP1ODataForm
}

// @Desc Build request message
func (o *DAACUDP1RequestForm) PackRequest(DAACUDP1I DAACUDP1I) (responseBody []byte, err error) {

	requestForm := DAACUDP1RequestForm{
		Form: []DAACUDP1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUDP1I",
				},
				FormData: DAACUDP1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUDP1RequestForm) UnPackRequest(request []byte) (DAACUDP1I, error) {
	DAACUDP1I := DAACUDP1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUDP1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUDP1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUDP1ResponseForm) PackResponse(DAACUDP1O DAACUDP1O) (responseBody []byte, err error) {
	responseForm := DAACUDP1ResponseForm{
		Form: []DAACUDP1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUDP1O",
				},
				FormData: DAACUDP1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUDP1ResponseForm) UnPackResponse(request []byte) (DAACUDP1O, error) {

	DAACUDP1O := DAACUDP1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUDP1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUDP1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUDP1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
