package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC100001I struct {
	AcctgAcctNo		string	`validate:"required"`	//核算账号
	Balancetype		string	`validate:"required"`	//（0-正常;1-逾期;2-减值）
	FixdIntrt		float64	`validate:"required"`	//固定利率
}

type AC100001O struct {
	Status	string
}

type AC100001IDataForm struct {
	FormHead CommonFormHead
	FormData AC100001I
}

type AC100001ODataForm struct {
	FormHead CommonFormHead
	FormData AC100001O
}

type AC100001RequestForm struct {
	Form []AC100001IDataForm
}

type AC100001ResponseForm struct {
	Form []AC100001ODataForm
}

// @Desc Build request message
func (o *AC100001RequestForm) PackRequest(AC100001I AC100001I) (responseBody []byte, err error) {

	requestForm := AC100001RequestForm{
		Form: []AC100001IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC100001I",
				},
				FormData: AC100001I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC100001RequestForm) UnPackRequest(request []byte) (AC100001I, error) {
	AC100001I := AC100001I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC100001I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC100001I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC100001ResponseForm) PackResponse(AC100001O AC100001O) (responseBody []byte, err error) {
	responseForm := AC100001ResponseForm{
		Form: []AC100001ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC100001O",
				},
				FormData: AC100001O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC100001ResponseForm) UnPackResponse(request []byte) (AC100001O, error) {

	AC100001O := AC100001O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC100001O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC100001O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC100001I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
