package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUBT0I struct {
	//输入是个map
}

type DAACUBT0O struct {

}

type DAACUBT0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUBT0I
}

type DAACUBT0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUBT0O
}

type DAACUBT0RequestForm struct {
	Form []DAACUBT0IDataForm
}

type DAACUBT0ResponseForm struct {
	Form []DAACUBT0ODataForm
}

// @Desc Build request message
func (o *DAACUBT0RequestForm) PackRequest(DAACUBT0I DAACUBT0I) (responseBody []byte, err error) {

	requestForm := DAACUBT0RequestForm{
		Form: []DAACUBT0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUBT0I",
				},
				FormData: DAACUBT0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUBT0RequestForm) UnPackRequest(request []byte) (DAACUBT0I, error) {
	DAACUBT0I := DAACUBT0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUBT0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUBT0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUBT0ResponseForm) PackResponse(DAACUBT0O DAACUBT0O) (responseBody []byte, err error) {
	responseForm := DAACUBT0ResponseForm{
		Form: []DAACUBT0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUBT0O",
				},
				FormData: DAACUBT0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUBT0ResponseForm) UnPackResponse(request []byte) (DAACUBT0O, error) {

	DAACUBT0O := DAACUBT0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUBT0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUBT0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUBT0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
