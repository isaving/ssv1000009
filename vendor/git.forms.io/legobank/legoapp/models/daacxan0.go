package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACXAN0I struct {
	AcctType   string ` validate:"required,max=10"`
}

type DAACXAN0O struct {
	AcctType   string
	Bescribe   string
	AccType    string
	Subject    string
	AccSubType string
	DrCrFlag   string
	SubFlag    string
	IsLedger   string
	IsAccount  string

}

type DAACXAN0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACXAN0I
}

type DAACXAN0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACXAN0O
}

type DAACXAN0RequestForm struct {
	Form []DAACXAN0IDataForm
}

type DAACXAN0ResponseForm struct {
	Form []DAACXAN0ODataForm
}

// @Desc Build request message
func (o *DAACXAN0RequestForm) PackRequest(DAACXAN0I DAACXAN0I) (responseBody []byte, err error) {

	requestForm := DAACXAN0RequestForm{
		Form: []DAACXAN0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACXAN0I",
				},
				FormData: DAACXAN0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACXAN0RequestForm) UnPackRequest(request []byte) (DAACXAN0I, error) {
	DAACXAN0I := DAACXAN0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACXAN0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACXAN0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACXAN0ResponseForm) PackResponse(DAACXAN0O DAACXAN0O) (responseBody []byte, err error) {
	responseForm := DAACXAN0ResponseForm{
		Form: []DAACXAN0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACXAN0O",
				},
				FormData: DAACXAN0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACXAN0ResponseForm) UnPackResponse(request []byte) (DAACXAN0O, error) {

	DAACXAN0O := DAACXAN0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACXAN0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACXAN0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACXAN0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
