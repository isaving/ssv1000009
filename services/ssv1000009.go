//Version: v0.0.1
package services

import (
	constants "git.forms.io/isaving/sv/ssv1000009/constant"
	"git.forms.io/isaving/sv/ssv1000009/models"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)
var Ssv1000009Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000009",
	ConfirmMethod: "ConfirmSsv1000009",
	CancelMethod:  "CancelSsv1000009",
}

type Ssv1000009 interface {
	TrySsv1000009(*models.SSV1000009I) (*models.SSV1000009O, error)
	ConfirmSsv1000009(*models.SSV1000009I) (*models.SSV1000009O, error)
	CancelSsv1000009(*models.SSV1000009I) (*models.SSV1000009O, error)
}

type Ssv1000009Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
    Sv100009O         *models.SSV1000009O
    Sv100009I         *models.SSV1000009I

}
// @Desc Ssv1000009 process
// @Author
// @Date 2020-12-04
func (impl *Ssv1000009Impl) TrySsv1000009(ssv1000009I *models.SSV1000009I) (ssv1000009O *models.SSV1000009O, err error) {

	impl.Sv100009I = ssv1000009I

	//AC000001 --新增核算账户信息
	if err :=impl.AC000001(); nil != err {
		return nil, err
	}

	ssv1000009O = &models.SSV1000009O{
		AccountingId: impl.Sv100009O.AccountingId,
		DcnId: impl.Sv100009O.DcnId,
	}

	return ssv1000009O, nil
}

func (impl *Ssv1000009Impl) ConfirmSsv1000009(ssv1000009I *models.SSV1000009I) (ssv1000009O *models.SSV1000009O, err error) {
	log.Debug("Start confirm ssv1000009")
	return nil, nil
}

func (impl *Ssv1000009Impl) CancelSsv1000009(ssv1000009I *models.SSV1000009I) (ssv1000009O *models.SSV1000009O, err error) {
	log.Debug("Start cancel ssv1000009")
	return nil, nil
}

func (impl *Ssv1000009Impl) AC000001() error {
	rspBody, err := impl.Sv100009I.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	//AC000001 --新增核算账户信息
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.AC000001, rspBody);
	if err != nil{
		log.Errorf("AC000001, err:%v",err)
		return err
	}
	Sv100009O := &models.SSV1000009O{}
	if err := Sv100009O.UnPackResponse(resBody); nil != err {
		return errors.New(err, constants.ERRCODE2)
	}
	impl.Sv100009O = Sv100009O
	return nil
}