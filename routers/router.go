////Version: v0.0.1
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv1000009/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-04
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.DefaultString(constant.TopicPrefix + "ssv1000009","SV100009"),
		&controllers.Ssv1000009Controller{}, "Ssv1000009")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-04
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv1000009",
		beego.NSNamespace("/ssv1000009",
			beego.NSInclude(
				&controllers.Ssv1000009Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
