//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/sv/ssv1000009/models"
	"git.forms.io/isaving/sv/ssv1000009/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000009Controller struct {
    controllers.CommTCCController
}

func (*Ssv1000009Controller) ControllerName() string {
	return "Ssv1000009Controller"
}

// @Desc Ssv1000009Controller Controller
// @Description Entry
// @Param Ssv1000009 body models.SSV1000009I true "body for model content"
// @Success 200 {object} models.SSV1000009O
// @router /SV100009 [post]
// @Date 2020-12-10
func (c *Ssv1000009Controller) Ssv1000009() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000009Controller.Ssv1000009 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000009I := &models.SSV1000009I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000009I); err != nil {
		c.SetServiceError(err)
		return
	}
  if err := ssv1000009I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}

	ssv1000009 := &services.Ssv1000009Impl{} 
   	ssv1000009.New(c.CommTCCController)
   	ssv1000009.Sv100009I = ssv1000009I
	//ssv1000009.DlsInterface = &commclient.DlsOperate{}
	ssv1000009Compensable := services.Ssv1000009Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000009, ssv1000009Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000009I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000009O, ok := rsp.(*models.SSV1000009O); ok {
		if responseBody, err := models.PackResponse(ssv1000009O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
} 
// @Title Ssv1000009 Controller
// @Description ssv1000009 controller
// @Param Ssv1000009 body models.SSV1000009I true body for SSV1000009 content
// @Success 200 {object} models.SSV1000009O
// @router /create [post]
func (c *Ssv1000009Controller) SWSsv1000009() {
	//Here is to generate API documentation, no need to implement methods
}
