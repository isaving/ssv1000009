//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package config

import (
	//"errors"
	log "git.forms.io/universe/comm-agent/common/log"
	"github.com/astaxie/beego"
	"github.com/go-errors/errors"
	"os"
	"strings"
	"sync"
)

const (
	DEPLOY_TYPE_CONTAINER = "C"
	DEPLOY_TYPE_BINARY    = "B"
)

// Global variable CommAgentCfg store all comm-agent configures
// it will be initialized by InitCommConfig function at startup.
var CommAgentCfg *CommAgentConfig

type LookupConfig struct {
	// IsEnable, if true means use arch-b, comm-agent lookup the dls to get DCN value of target service
	// if false means use arch-a, comm-agent never lookup the dls, and if target DCN is empty, the request will failed.
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	IsEnable bool

	// CommonDcn,for arch-b, if a request id not das then set the target dcn to this value
	// this value must be defined in config file
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	CommonDcn string

	// DASPrefix,The prefix of the DAS, the system will determine whether the requested
	// target service is a DAS service according to whether the prefix of the topic is equal to the value.
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	DasPrefix string
}

type ClientConfig struct {
	// CallbackUrl, the http url for comm-agent server callback application when it's receiving a message from
	// solace broker
	CallbackUrl string

	StatusCheckUrl string

	CallbackTimeoutSeconds int

	CallbackParallelNumber int

	CallbackChanSize int
}

type MqSessionConfig struct {
	ConfigFilePath string
}

type AppConfig struct {
	// Org, The organization to which the current service belongs
	// if it's not set or value is empty,it will raise a panic when comm-agent server startup.
	Org string

	// Az, The available zone to which the current service belongs
	// if it's not set or value is empty,it will raise a panic when comm-agent server startup.
	Az string

	// Dcn, The data center node to which the current service belongs
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	Dcn string

	// NodeId, the node to which the current services belongs
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	NodeId string

	// ServiceId, define the service id of current service.
	// if it's not set or value if empty, it will raise a panic when comm-agent server startup.
	ServiceId string

	InstanceId string

	SendRequestReplyTimeoutSeconds int

	IsEnableClientHealthStatCheck bool

	IsPersistentDeliveryMode bool

	IsEnableAppendGlobalTxnIdToTopic bool

	ExclusionsAppendGlobalTxnIdTopics map[string]interface{}

	IsAutoAckForAsyncCallback bool
}

type CryptoConfig struct {
	// IsEnable, if true
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	IsEnable bool

	// Algo, define the algorithm of crypto when enable message payload crypto
	// if server is enable the crypto flag and this value is not set or value is empty,
	// it will raise a panic when comm-agent server startup.
	Algo string

	// Mode, define the mode of crypto when enable message payload crypto
	// if server is enable the crypto flag and this value is not set or value is empty,
	// it will raise a panic when comm-agent server startup.
	Mode string

	// Padding, define the padding type of crypto when enable message payload crypto
	// if server is enable the crypto flag and this value is not set or value is empty,
	// it will raise a panic when comm-agent server startup.
	Padding string

	KmsCryptoAlgorithm string

	upstreamServices []string
	UpstreamRWLock   sync.RWMutex

	downstreamServices []string
	DownstreamRWLock   sync.RWMutex

	ExclusionsTopics           map[string]interface{}
	GetServicesKeysTopic       string
	RotateServicesKeysTopic    string
	ServiceKeyActiveHours      int
	KeyGenerationTimeInAdvance int
	KmsPubKeyPath              string

	// the period of key upgrade
	PeriodOfKeyUpgradeCheck int

	SpecificKeyId  string
	SpecificTopics map[string]interface{}
}

func (c *CryptoConfig) GetUpstreamServices() []string {
	c.UpstreamRWLock.RLock()
	defer c.UpstreamRWLock.RUnlock()

	return c.upstreamServices
}

func (c *CryptoConfig) SetUpstreamServices(iUpstreamServices []string) {
	c.UpstreamRWLock.Lock()
	defer c.UpstreamRWLock.Unlock()

	c.upstreamServices = iUpstreamServices
}

func (c *CryptoConfig) GetDownstreamServices() []string {
	c.DownstreamRWLock.RLock()
	defer c.DownstreamRWLock.RUnlock()

	return c.downstreamServices
}

func (c *CryptoConfig) SetDownstreamServices(iDownstreamServices []string) {
	c.DownstreamRWLock.Lock()
	defer c.DownstreamRWLock.Unlock()

	c.downstreamServices = iDownstreamServices
}

type DeploymentConfig struct {
	IsEnableSecure bool
	Mode           string
	// fixed filling `0xF25EE7CB`
	MagicNumber string
	Key         string

	PluginPath string
}

type ApmConfig struct {
	// IsEnable, if true comm-agent server will logging the apm log
	// if it's not set or value is empty, it will raise a panic when comm-agent server startup.
	IsEnable bool
	RootPath string
	FileRows int
}

type CommAgentConfig struct {
	// ReplyChannelNum, the maximum number of channels available for R/R message synchronization
	// default value is 1024
	ReplyChannelNum int

	// ServerAddr, define the http ip address of comm-agent server
	// default is "0.0.0.0"
	ServerAddr string

	// ServerPort, define the http port of comm-agent server
	// default is "18080"
	ServerPort string

	App AppConfig

	Apm ApmConfig

	Crypto CryptoConfig

	Lookup LookupConfig

	Deployment DeploymentConfig

	Client ClientConfig
	// the mode of deployment
	//DeployMode string

	IsStarted bool

	lock sync.RWMutex

	appIsStartCheckLock sync.RWMutex
	appIsStartCheck     bool
}

func checkConfigWhetherExists(keys []string) error {
	for _, key := range keys {
		if "" == beego.AppConfig.String(key) {
			return errors.Errorf("cannot found key:`%s` in config file!", key)
		}
	}

	return nil
}

func loadCryptoConfig() error {
	log.Infof("start load crypto config...")
	var err error
	crypto := &CryptoConfig{}
	if "" == beego.AppConfig.String("crypto::enable") {
		return errors.New("cannot found key: `crypto::enable` in config file!")
	}

	if crypto.IsEnable, err = beego.AppConfig.Bool("crypto::enable"); nil != err {
		return errors.Wrap(err, 0)
	}

	if !crypto.IsEnable {
		return nil
	}

	if err := checkConfigWhetherExists([]string{
		"crypto::algo",
		"crypto::mode",
		"crypto::padding",
		"crypto::getServicesKeysTopic",
		"crypto::rotateServicesKeysTopic",
	}); nil != err {
		return err
	}

	crypto.Algo = beego.AppConfig.String("crypto::algo")
	crypto.Mode = beego.AppConfig.String("crypto::mode")
	crypto.Padding = beego.AppConfig.String("crypto::padding")
	crypto.KmsCryptoAlgorithm = beego.AppConfig.DefaultString("crypto::kmsCryptoAlgorithm", "RSA")
	crypto.upstreamServices = beego.AppConfig.Strings("crypto::upstreamServices")

	exclusionsTopics := beego.AppConfig.Strings("crypto::exclusionsTopics")
	crypto.ExclusionsTopics = make(map[string]interface{})
	if len(exclusionsTopics) > 0 {
		for index, topic := range exclusionsTopics {
			log.Debugf("exclusionsTopics key [%d]: %s", index, topic)
			crypto.ExclusionsTopics[topic] = nil
		}
	}

	crypto.GetServicesKeysTopic = beego.AppConfig.String("crypto::getServicesKeysTopic")
	crypto.RotateServicesKeysTopic = beego.AppConfig.String("crypto::rotateServicesKeysTopic")

	if crypto.ServiceKeyActiveHours, err = beego.AppConfig.Int("crypto::serviceKeyActiveHours"); nil != err {
		return errors.Wrap(err, 0)
	}

	if crypto.KeyGenerationTimeInAdvance, err = beego.AppConfig.Int("crypto::keyGenerationTimeInAdvance"); nil != err {
		return errors.Wrap(err, 0)
	}

	crypto.KmsPubKeyPath = beego.AppConfig.DefaultString("crypto::kmsPubKeyPath", "./conf/kms_pub.pem")
	crypto.SpecificKeyId = beego.AppConfig.String("crypto::specificKey")

	specificTopics := beego.AppConfig.Strings("crypto::specificTopics")
	crypto.SpecificTopics = make(map[string]interface{})
	if len(specificTopics) > 0 {
		for index, topic := range specificTopics {
			log.Debugf("specificTopics key [%d]: %s", index, topic)
			crypto.SpecificTopics[topic] = nil
		}
	}

	// default period is 60 seconds
	crypto.PeriodOfKeyUpgradeCheck = beego.AppConfig.DefaultInt("crypto::checkPeriod", 60)

	CommAgentCfg.Crypto = *crypto

	log.Infof("loaded crypto config successfully!")

	return nil
}

func loadLookupConfig() error {
	log.Infof("start load lookup config...")
	var err error
	lookup := &LookupConfig{}

	if "" == beego.AppConfig.String("lookup::enable") {
		return errors.New("cannot found key:`lookup::enable` in config file!")
	}

	if lookup.IsEnable, err = beego.AppConfig.Bool("lookup::enable"); nil != err {
		return errors.Wrap(err, 0)
	}

	if !lookup.IsEnable {
		return nil
	}

	lookup.CommonDcn = beego.AppConfig.DefaultString("lookup::commonDcn", "000000")
	lookup.DasPrefix = beego.AppConfig.DefaultString("lookup::dasPrefix", "DA")

	CommAgentCfg.Lookup = *lookup

	log.Infof("loaded lookup config successfully!")

	return nil
}

func loadApmConfig() error {
	log.Infof("start load APM config...")
	var err error
	apm := &ApmConfig{}

	if "" == beego.AppConfig.String("apm::enable") {
		return errors.New("cannot found key:`apm::enable` in config file!")
	}

	if apm.IsEnable, err = beego.AppConfig.Bool("apm::enable"); nil != err {
		return errors.Wrap(err, 0)
	}

	if !apm.IsEnable {
		return nil
	}

	fileRows := beego.AppConfig.DefaultInt("apm::fileRows", 1000000)
	apm.FileRows = fileRows

	rootPath := beego.AppConfig.DefaultString("apm::rootPath", "/data/commlogs/")
	apm.RootPath = rootPath
	if !strings.HasSuffix(rootPath, "/") {
		apm.RootPath += "/"
	}

	CommAgentCfg.Apm = *apm

	log.Infof("loaded APM config successfully!")
	return nil
}

func loadDeploymentConfig() error {
	log.Infof("start load deployment config...")
	var err error
	deployment := &DeploymentConfig{}

	if "" == beego.AppConfig.String("deployment::enableSecure") {
		return errors.New("cannot found key:`deployment::enableSecure` in config file!")
	}

	if deployment.IsEnableSecure, err = beego.AppConfig.Bool("deployment::enableSecure"); nil != err {
		return errors.Wrap(err, 0)
	}

	if !deployment.IsEnableSecure {
		return nil
	}

	deployment.Mode = strings.ToUpper(beego.AppConfig.DefaultString("deployment::mode", "C"))
	deployment.MagicNumber = "0xF25EE7CB"
	deployment.Key = beego.AppConfig.String("deployment::key")
	deployment.PluginPath = beego.AppConfig.DefaultString("deployment::pluginPath", ".GK")
	CommAgentCfg.Deployment = *deployment

	log.Infof("loaded deployment config successfully!")
	return nil
}

func loadAppConfig() error {
	log.Infof("start load app config...")
	app := &AppConfig{}

	app.Org = beego.AppConfig.String("app::org")
	app.Az = beego.AppConfig.String("app::az")
	app.Dcn = beego.AppConfig.String("app::dcn")
	app.ServiceId = beego.AppConfig.String("app::serviceId")
	app.NodeId = os.Getenv("NODE_ID")
	instanceID := os.Getenv("INSTANCE_ID")
	if "" == instanceID {
		return errors.New(" failed to get env:INSTANCE_ID")
	}
	app.InstanceId = instanceID
	app.IsEnableClientHealthStatCheck = beego.AppConfig.DefaultBool("app::enableClientHealthStatCheck", false)
	app.IsPersistentDeliveryMode = beego.AppConfig.DefaultBool("app::persistentDeliveryMode", false)
	app.IsEnableAppendGlobalTxnIdToTopic = beego.AppConfig.DefaultBool("app::enableAppendGlobalTxnIdToTopic", false)

	exclusionsTopics := beego.AppConfig.Strings("app::exclusionsAppendGlobalTxnIdTopics")
	app.ExclusionsAppendGlobalTxnIdTopics = make(map[string]interface{})
	if len(exclusionsTopics) > 0 {
		for index, topic := range exclusionsTopics {
			log.Debugf("exclusions append Global Txn Id Topic key [%d]: %s", index, topic)
			app.ExclusionsAppendGlobalTxnIdTopics[topic] = nil
		}
	}

	app.SendRequestReplyTimeoutSeconds = beego.AppConfig.DefaultInt("app::sendRequestReplyTimeoutSeconds", 30)
	app.IsAutoAckForAsyncCallback = beego.AppConfig.DefaultBool("app::autoAckForAsyncCallback", true)

	CommAgentCfg.App = *app

	log.Infof("loaded app config successfully!")
	return nil
}

func invokeLoadConfigFunc(functions []func() error) error {
	for _, f := range functions {
		if err := f(); nil != err {
			return err
		}
	}

	return nil
}

func InitCommConfig() (err error) {
	log.Info("start init. CommConfig...")
	CommAgentCfg = &CommAgentConfig{}
	CommAgentCfg.ReplyChannelNum = beego.AppConfig.DefaultInt("replyChannelNum", 1024)
	CommAgentCfg.ServerAddr = beego.AppConfig.DefaultString("httpaddr", "0.0.0.0")
	CommAgentCfg.ServerPort = beego.AppConfig.DefaultString("httpport", "18080")

	client := &ClientConfig{}
	client.CallbackUrl = beego.AppConfig.String("client::callbackUrl")
	client.StatusCheckUrl = beego.AppConfig.DefaultString("client::statusCheckUrl", "http://127.0.0.1:18082/v1/client/status")
	client.CallbackTimeoutSeconds = beego.AppConfig.DefaultInt("client::callbackTimeoutSeconds", 30)
	client.CallbackParallelNumber = beego.AppConfig.DefaultInt("client::callbackParallelNumber", 128)
	client.CallbackChanSize = beego.AppConfig.DefaultInt("client::callbackChanSize", 1024)

	CommAgentCfg.Client = *client

	if err := invokeLoadConfigFunc([]func() error{
		// load config for app
		loadAppConfig,

		// load config for lookup
		loadLookupConfig,

		// load config for APM
		loadApmConfig,

		// load config for deployment
		loadDeploymentConfig,

		// load config for crypto
		loadCryptoConfig,
	}); nil != err {
		return err
	}

	log.Infof("init comm-agent config successfully, CommAgentConfig=[%++v]!", CommAgentCfg)
	return
}

func SetIsStarted() {
	CommAgentCfg.lock.Lock()
	defer CommAgentCfg.lock.Unlock()

	CommAgentCfg.IsStarted = true
}

func IsStarted() bool {
	CommAgentCfg.lock.RLock()
	defer CommAgentCfg.lock.RUnlock()

	return CommAgentCfg.IsStarted
}

func SetClientStat(stat bool) {
	CommAgentCfg.appIsStartCheckLock.Lock()
	defer CommAgentCfg.appIsStartCheckLock.Unlock()

	CommAgentCfg.appIsStartCheck = stat
}

func IsClientActive() bool {
	CommAgentCfg.appIsStartCheckLock.RLock()
	defer CommAgentCfg.appIsStartCheckLock.RUnlock()

	return CommAgentCfg.appIsStartCheck
}
